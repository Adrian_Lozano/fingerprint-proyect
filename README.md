# Fingerprint Proyect

**Crear tablas**

Ejecutar los comandos en el archivo CreateSchemaAndTables.txt
Nota: se borra y se crea un schema llamado checker, de igual manera se borran y 
se crean las tablas "employee", "employeeworkday" y "work_day".

**Spring MVC**

[http://localhost:8080/Momentum/MVC/](url)


**Inicializar la base de datos**
Para llenar la base de datos ir a [](http://localhost:8080/Momentum/api/v1/init), 
con el archivo en la ruta C:\\apache-tomcat-8.5.42\\Registros Momentum.xls

**Endpoint 1**
Sustituir {is}, {mes} por la consulta deseada

[http://localhost:8080/Momentum/api/v1/users/{is}/{mes}](url)

**Endpoint 2**

[http://localhost:8080/Momentum/api/v1/users](url)


**Endpoint 3**
Sustituir {mes} por la consulta deseada

[http://localhost:8080/Momentum/api/v1/period/{mes}](url)


**Endpoint 4**

[http://localhost:8080/Momentum/api/v1/period](url)