package com.softtek.academy.momentum.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.softtek.academy.momentum.interfaces.EmployeeService;
import com.softtek.academy.momentum.model.Employee;

@SpringBootTest
public class ServiceEmployeeTest {
	@Autowired
	EmployeeService employeeService;
	
	@Test
	public void testGetHours() {
		String name = "OCO1";
		String month = "02";
		String expectedHours = "94:56:54";
		String actualHours = employeeService.getHours(name, month);
		assertNotNull(actualHours);
		assertEquals(expectedHours, actualHours);
	}
	@Test
	public void testGetAllByHours() {
		String month = "02";
		List<Employee> listEmployees = employeeService.getAllByMonth(month);
		
		String expectedCheckIn = "2019-02-19 07:55:08";
		String actualCheckIn = listEmployees.get(0).getWorkDays()
			.get(0).getCheckIn().format(DateTimeFormatter
			.ofPattern("yyyy-MM-dd HH:mm:ss"))
			.toString();
		assertNotNull(listEmployees);
		assertEquals(expectedCheckIn, actualCheckIn);
	}
	
}
