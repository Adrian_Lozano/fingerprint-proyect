package com.softtek.academy.momentum.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.softtek.academy.momentum.interfaces.ExcelFilterService;
import com.softtek.academy.momentum.model.Employee;
import com.softtek.academy.momentum.model.TableBean;

@SpringBootTest
public class ServiceExcelTest {
	@Autowired
	ExcelFilterService excelFilterService;
	
	@Test
	public void testLoadFile() {
		List<TableBean> fileLoaded = new ArrayList<>();
		try {
			fileLoaded = excelFilterService.loadFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String nameExpected = "CASV1";
		String nameActual = fileLoaded.get(0).getIs();
		assertNotNull(fileLoaded);
		assertEquals(nameExpected, nameActual);
	}
	@Test
	public void testFilterEmployee() {
		List<Employee> fileLoaded = new ArrayList<>();
		try {
			fileLoaded = excelFilterService.filter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String nameExpected = "MFFR";
		String nameActual = fileLoaded.get(0).getName();
		assertNotNull(fileLoaded);
		assertEquals(nameExpected, nameActual);
	}
}
