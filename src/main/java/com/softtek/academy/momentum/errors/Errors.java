package com.softtek.academy.momentum.errors;

import java.util.LinkedHashMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Errors {
	private final static LinkedHashMap<String, String> response = new LinkedHashMap<>();

	public ResponseEntity<Object> errorIsNotFound(String is) {
		response.clear();
		response.put("Error", "Oops...");
		response.put("Parameter", "is = " + is);
		response.put("Message", "No existe el is");
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	public ResponseEntity<Object> errorIsWithNotMonth(String is, String mes) {
		response.clear();
		response.put("Error", "Oops...");
		response.put("Reason", "El is " + is + " no tiene registros en el mes " + mes);
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	public ResponseEntity<Object> errorWrongMonth(String mes) {
		response.clear();
		response.put("Error", "Oops...");
		response.put("Parameter", "mes = " + mes);
		response.put("Message", "El parametro debe estar entre 01 y 12 ");
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	public ResponseEntity<Object> errorEmptyWorkDays(String is, String start, String end) {
		response.clear();
		response.put("Error", "Oops...");
		response.put("Reason", "El is " + is + " no tiene registros entre " + start + " y " + end);
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	public ResponseEntity<Object> errorAllByMonthEmpty(String mes) {
		response.clear();
		response.put("Error", "Oops...");
		response.put("Reason", "No existen registros en el mes " + mes);
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	public ResponseEntity<Object> errorAllByPeriod(String start, String end) {
		response.clear();
		response.put("Error", "Oops...");
		response.put("Reason", "No existen registros entre " + start + " y " + end);
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}
	
	public ResponseEntity<Object> errorDateFormat(){
		response.clear();
		response.put("Error", "Oops...");
		response.put("Reason", "El formato de fecha debe ser yyyy-MM-dd");
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

}
