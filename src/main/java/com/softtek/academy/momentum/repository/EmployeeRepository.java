package com.softtek.academy.momentum.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.softtek.academy.momentum.model.Employee;
@Repository("EmployeeRepository")
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
	Employee findByName(String name);
//	List<Employee> findByCheckOutBetween(String start, String end);
}
