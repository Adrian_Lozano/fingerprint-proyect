package com.softtek.academy.momentum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.softtek.academy.momentum.model.Employee;
@Repository("ExcelRepository")
public interface ExcelRepository extends JpaRepository<Employee, Long> {

}
