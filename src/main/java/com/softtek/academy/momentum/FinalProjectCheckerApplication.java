package com.softtek.academy.momentum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalProjectCheckerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalProjectCheckerApplication.class, args);
	}

}
