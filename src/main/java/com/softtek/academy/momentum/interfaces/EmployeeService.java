package com.softtek.academy.momentum.interfaces;

import java.text.ParseException;
import java.util.List;

import com.softtek.academy.momentum.model.Employee;

public interface EmployeeService {
	String getHours(String name, String month);
	 Employee getEmployeePeriod(String name, String start, String end);
	 List<Employee> getAllByMonth(String month);
	 List<Employee> getByPeriod(String start, String end) throws ParseException;
}
