package com.softtek.academy.momentum.interfaces;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;

import com.softtek.academy.momentum.model.Employee;
import com.softtek.academy.momentum.model.TableBean;

public interface ExcelFilterService {
	List<Employee> filter() throws IOException;
	List<TableBean> saveInTableBean(Iterator<Row> iterator);
	List<Employee> saveInBd(Map<String, List<TableBean>> divMap);
	List<TableBean> loadFile() throws IOException; 
}
