package com.softtek.academy.momentum.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.momentum.interfaces.ExcelFilterService;
import com.softtek.academy.momentum.model.Employee;

@RestController
public class ExcelController {
	@Autowired
	private ExcelFilterService excelService;

	public ExcelFilterService getExcelService() {
		return excelService;
	}

	public ExcelController(ExcelFilterService excelService) {
		this.excelService = excelService;
	}

	@GetMapping(value = "api/v1/init")
	public List<Employee> init() throws IOException {
		return excelService.filter();
	}
}
