package com.softtek.academy.momentum.controller;

import java.io.IOException;
import java.text.ParseException;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.softtek.academy.momentum.interfaces.EmployeeService;
import com.softtek.academy.momentum.interfaces.ExcelFilterService;
import com.softtek.academy.momentum.model.Employee;

@Controller
@RequestMapping("/MVC")
public class MVCController {
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private ExcelFilterService excelService;

	public ExcelFilterService getExcelService() {
		return excelService;
	}
	public MVCController(EmployeeService employeeService) {

		this.employeeService = employeeService;
	}
	
	@RequestMapping(value = "/")
	public String index() {
		return "views/index";
	}
	
	@RequestMapping(value = "/IsMonth")
	public String IsMonthForm() {
		return "views/IsMonthForm";
	}
	@RequestMapping(value = "/IsPeriod")
	public String IsPeriodForm() {
		return "views/IsPeriodForm";
	}
	
	@RequestMapping(value = "/AllMonth")
	public String AllMonthForm() {
		return "views/AllMonthForm";
	}
	
	@RequestMapping(value = "/AllPeriod")
	public String AllPeriodForm() {
		return "views/AllPeriodForm";
	}
	
	@RequestMapping(value = "/init")
	public String init(Model model)  {
		try {
			excelService.filter();
		} catch (IOException e) {
			model.addAttribute("error", "App can't be init...");
			return "redirect:/MVC/";
		}
		model.addAttribute("success", "App inited");
		return "redirect:/MVC/";
	}
	
	@GetMapping(value = "/users")
	public String getEmployeeMonth(String is, String month, Model model) {
		
		String horas = "";
		if (Integer.parseInt(month) > 12 || Integer.parseInt(month) < 1) {
			model.addAttribute("error", "Month must be between 01-12");
			return "views/IsMonthForm";
		}
		try {
			horas = employeeService.getHours(is, month);
			if (horas.equals("00:00:00")) {
				model.addAttribute("error", is +" Don't have records in month: " + month);
				return "views/IsMonthForm";
			}
		} catch (NullPointerException e) {
			model.addAttribute("error", is + " Not Found");
			return "views/IsMonthForm";
		}
		int mesInt = Integer.parseInt(month);
		model.addAttribute("is", is);
		model.addAttribute("Month", Integer.toString(mesInt));
		model.addAttribute("Hours", horas);
		return "views/ViewHours";
	}
	@PostMapping(value = "/users")
	public String getEmployeePeriod(String is, String start, String end, Model model) {
		Employee employee = null;
		try {
			employee = employeeService.getEmployeePeriod(is, start, end);
			if (employee.getWorkDays().isEmpty()) {
				model.addAttribute("error", is +" Don't have records between: " + start + " and " + end);
				return "views/IsPeriodForm";
			}
		} catch (NullPointerException e) {
			model.addAttribute("error", is + " Not Found");
			return "views/IsPeriodForm";
		} catch (DateTimeParseException x) {
			model.addAttribute("error", "The format of start and end must be yyyy-MM-dd including middle dash");
			return "views/IsPeriodForm";
		}
		model.addAttribute("emp", employee);
		return "views/ViewIsPeriod";
	}
	
	@GetMapping(value = "/period")
	public String getAllByMonth(String month, Model model) {

		if (Integer.parseInt(month) > 12 || Integer.parseInt(month) < 1) {
			model.addAttribute("error", "Month must be between 01-12");
			return "views/AllMonthForm";
		}
		List<Employee> employees = employeeService.getAllByMonth(month);
		if (employees.isEmpty()) {
			model.addAttribute("error", "No records found in month: " + month);
			return "views/AllMonthForm";
		}
		model.addAttribute("employees", employees);
		return "views/ViewAllMonth";
	}

	@PostMapping("/period")
	public String getAllPeriod(String start, String end, Model model) throws ParseException {
		List<Employee> employees = new ArrayList<>();
		try {
			employees = employeeService.getByPeriod(start, end);
			if (employees.isEmpty()) {
				model.addAttribute("error", "No records between: " + start + " and " + end);
				return "views/AllPeriodForm";
			}
		} catch (DateTimeParseException x) {
			model.addAttribute("error", "The format of start and end must be yyyy-MM-dd including middle dash");
			return "views/AllPeriodForm";
		}
		model.addAttribute("employees", employees);
		return "views/ViewAllPeriod";
	}

}
