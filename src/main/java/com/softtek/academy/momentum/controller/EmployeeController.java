package com.softtek.academy.momentum.controller;

import java.text.ParseException;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.momentum.errors.Errors;
import com.softtek.academy.momentum.interfaces.EmployeeService;
import com.softtek.academy.momentum.model.Employee;

@RequestMapping("/api/v1")
@RestController
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;
	private Errors errors = new Errors();

	public EmployeeController(EmployeeService employeeService) {

		this.employeeService = employeeService;
	}

	@GetMapping(value = "/users/{is}/{month}")
	public ResponseEntity<Object> getEmployeeMonth(@PathVariable String is, @PathVariable String month) {
		
		LinkedHashMap<String, String> response = new LinkedHashMap<>();
		String horas = "";
		if (Integer.parseInt(month) > 12 || Integer.parseInt(month) < 1) {
			return errors.errorWrongMonth(month);
		}
		try {
			horas = employeeService.getHours(is, month);
			if (horas.equals("00:00:00")) {
				return errors.errorIsWithNotMonth(is, month);
			}
		} catch (NullPointerException e) {

			return errors.errorIsNotFound(is);
		}
		int mesInt = Integer.parseInt(month);
		response.put("is", is);
		response.put("Mes", Integer.toString(mesInt));
		response.put("Horas", horas);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping(value = "/users")
	public ResponseEntity<Object> getEmployeePeriod(String is, String start, String end) {
		Employee employee = null;
		try {
			employee = employeeService.getEmployeePeriod(is, start, end);
			if (employee.getWorkDays().isEmpty()) {
				return errors.errorEmptyWorkDays(is, start, end);
			}
		} catch (NullPointerException e) {
			return errors.errorIsNotFound(is);
		} catch (DateTimeParseException x) {
			return errors.errorDateFormat();
		}
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@GetMapping(value = "/period/{month}")
	public ResponseEntity<Object> getAllByMonth(@PathVariable String month) {

		if (Integer.parseInt(month) > 12 || Integer.parseInt(month) < 1) {
			return errors.errorWrongMonth(month);
		}
		List<Employee> employees = employeeService.getAllByMonth(month);
		if (employees.isEmpty()) {
			return errors.errorAllByMonthEmpty(month);
		}
		return new ResponseEntity<>(employees, HttpStatus.OK);
	}

	@PostMapping("/period")
	public ResponseEntity<Object> getAllPeriod(String start, String end) throws ParseException {
		List<Employee> employees = new ArrayList<>();
		try {
			employees = employeeService.getByPeriod(start, end);
			if (employees.isEmpty()) {
				return errors.errorAllByPeriod(start, end);
			}
		} catch (DateTimeParseException x) {
			return errors.errorDateFormat();
		}

		return new ResponseEntity<>(employees, HttpStatus.OK);
	}

}
