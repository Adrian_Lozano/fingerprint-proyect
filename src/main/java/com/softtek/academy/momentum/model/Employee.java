package com.softtek.academy.momentum.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_employee", unique = true , nullable = false)
	private int idEmployee;
	private String name;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name="employeeworkday", 
	joinColumns =  @JoinColumn(name = "idEmployee") ,
			inverseJoinColumns = @JoinColumn(name = "idWorkDay"))
	private List<WorkDay> workDays;

	public Employee() {
		
	}
	
	public int getIdEmp() {
		return idEmployee;
	}
	public void setIdEmp(int idEmp) {
		this.idEmployee = idEmp;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<WorkDay> getWorkDays() {
		return workDays;
	}
	public void setWorkDays(List<WorkDay> workDays) {
		this.workDays = workDays;
	}

}
