package com.softtek.academy.momentum.model;

public class TableBean {
	private String is;
	private String date;
	private String checkInOut;

	public String getIs() {
		return is;
	}

	public void setIs(String is) {
		this.is = is;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCheckInOut() {
		return checkInOut;
	}

	public void setCheckInOut(String checkInOut) {
		this.checkInOut = checkInOut;
	}

}
