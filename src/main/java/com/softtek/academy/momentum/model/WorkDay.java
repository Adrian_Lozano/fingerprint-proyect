package com.softtek.academy.momentum.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@Entity
public class WorkDay {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idWorkDay")
	private int idWorkDay;

	@Column(name = "check_in", length = 45)

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime checkIn;
	@Column(name = "check_out", length = 45)

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime checkOut;

	public WorkDay() {

	}

	public int getIdWorkDay() {
		return idWorkDay;
	}

	public void setIdWorkDay(int idWorkDay) {
		this.idWorkDay = idWorkDay;
	}

	public LocalDateTime getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(String checkIn) {

		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//			format.setTimeZone(TimeZone.getTimeZone("America/Mexico City"));
		this.checkIn = LocalDateTime.parse(checkIn, format);
	}

	public LocalDateTime getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		this.checkOut = LocalDateTime.parse(checkOut, format);

	}

}
