package com.softtek.academy.momentum.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.softtek.academy.momentum.interfaces.ExcelFilterService;
import com.softtek.academy.momentum.model.Employee;
import com.softtek.academy.momentum.model.TableBean;
import com.softtek.academy.momentum.model.WorkDay;
import com.softtek.academy.momentum.repository.ExcelRepository;

@Service
public class ExcelFilterServiceImpl implements ExcelFilterService{
	@Autowired
	@Qualifier("ExcelRepository")
	private ExcelRepository jpaExcelRepository;
	
	public ExcelFilterServiceImpl(ExcelRepository jpaExcelRepository) {
		this.jpaExcelRepository = jpaExcelRepository;
	}

	public List<TableBean> loadFile() throws IOException {
		String excel = "C:\\apache-tomcat-8.5.42\\Registros Momentum.xls";
		FileInputStream file = new FileInputStream(new File(excel));
		@SuppressWarnings("resource")
		Workbook workBook = new HSSFWorkbook(file);
		Sheet firstSheet = workBook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();

		iterator.next();

		return saveInTableBean(iterator);
	}
	public List<TableBean> saveInTableBean(Iterator<Row> iterator){
		List<TableBean> table = new ArrayList<>();

		while (iterator.hasNext()) {
			TableBean bean = new TableBean();
			Row nextRow = iterator.next();
			bean.setIs(nextRow.getCell(2).getStringCellValue());
			bean.setDate(nextRow.getCell(4).getStringCellValue());
			bean.setCheckInOut(nextRow.getCell(6).getStringCellValue());
			table.add(bean);
		}
		return table;
	}
	public List<Employee> saveInBd(Map<String, List<TableBean>> divMap){
		List<Employee> employees = new ArrayList<>();
		jpaExcelRepository.deleteAll();
		for (Map.Entry<String, List<TableBean>> entry : divMap.entrySet()) {
			Employee emplo = new Employee();
			emplo.setName(entry.getKey());
			List<WorkDay> workDays = new ArrayList<>();
			List<TableBean> workBean = entry.getValue();
			for (int i = 0; i < workBean.size() - 1; i+=2) {
				WorkDay workDay = new WorkDay();
				workDay.setCheckIn(workBean.get(i).getDate());
				workDay.setCheckOut(workBean.get(i + 1).getDate());
				workDays.add(workDay);
			}
			emplo.setWorkDays(workDays);
			jpaExcelRepository.save(emplo);	
			employees.add(emplo);
		}
		return employees;
	}
	public List<Employee> filter() throws IOException {
		List<TableBean> table = loadFile();
		List<TableBean> filtredTable = new ArrayList<>();
		table.sort(Comparator.comparing(TableBean::getIs).thenComparing(TableBean::getDate)
				.thenComparing(TableBean::getCheckInOut));
		Set<String> allIs = new HashSet<>();
		table.stream().filter(e -> allIs.add(e.getIs())).collect(Collectors.toList());

		for (int i = 0; i < table.size() - 1; i++) {
			if (table.get(i).getIs().equals(table.get(i + 1).getIs())) {

				String currentDate = table.get(i).getDate().substring(0, 10);
				String nextDate = table.get(i + 1).getDate().substring(0, 10);
				if (currentDate.equals(nextDate)) {
					String currentCheck = table.get(i).getCheckInOut();
					String nextCheck = table.get(i + 1).getCheckInOut();
					if (currentCheck.contains("In") && nextCheck.contains("Out")) {
						int currentHour = Integer.parseInt(table.get(i).getDate().substring(11, 13));
						int nextHour = Integer.parseInt(table.get(i + 1).getDate().substring(11, 13));
						if (nextHour > currentHour) {

							filtredTable.add(table.get(i));
							filtredTable.add(table.get(i + 1));
							i++;
						}
					} else {
						int currentHour = Integer.parseInt(table.get(i).getDate().substring(11, 13));
						int nextHour = Integer.parseInt(table.get(i + 1).getDate().substring(11, 13));
						if (nextHour > currentHour) {
							filtredTable.add(table.get(i));
							filtredTable.add(table.get(i + 1));
							i++;
						}
					}

				}
			}
		}
		Map<String, List<TableBean>> divMap = filtredTable.stream().flatMap(tableBean -> {
			Map<String, TableBean> um = new HashMap<>();
			um.put(tableBean.getIs(), tableBean);
			return um.entrySet().stream();
		}).collect(
				Collectors.groupingBy(Map.Entry::getKey, 
				Collectors.mapping(Map.Entry::getValue, 
				Collectors.toList())));
		List<TableBean> empty = new ArrayList<>();
		for (String t : allIs) {
			if(!(divMap.containsKey(t))) {
				divMap.put(t, empty);
			}
		}
		
		List<Employee> employees = saveInBd(divMap);
		
		return employees;
	}
}
