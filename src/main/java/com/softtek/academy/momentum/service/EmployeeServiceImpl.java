package com.softtek.academy.momentum.service;

import java.text.ParseException;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.softtek.academy.momentum.interfaces.EmployeeService;
import com.softtek.academy.momentum.model.Employee;
import com.softtek.academy.momentum.model.WorkDay;
import com.softtek.academy.momentum.repository.EmployeeRepository;;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	@Qualifier("EmployeeRepository")
	private EmployeeRepository jpaEmployeeRepository;

	public EmployeeServiceImpl(EmployeeRepository jpaEmployeeRepository) {
		this.jpaEmployeeRepository = jpaEmployeeRepository;
	}

	@Override
	public String getHours(String name, String month) {
		Employee emp = jpaEmployeeRepository.findByName(name);
		emp.getWorkDays().removeIf(w -> !(w.getCheckIn().getMonthValue() == Integer.parseInt(month)));
		long millis = 0;
		for (WorkDay workDay : emp.getWorkDays()) {
			millis += workDay.getCheckIn().until(workDay.getCheckOut().atZone(ZoneId.of("America/Mexico_City")),
					ChronoUnit.MILLIS);
		}

		String hours = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
				TimeUnit.MILLISECONDS.toMinutes(millis)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
				TimeUnit.MILLISECONDS.toSeconds(millis)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

		return hours;
	}

	@Override
	public Employee getEmployeePeriod(String name, String start, String end) {
		Employee emp = jpaEmployeeRepository.findByName(name);
		start = start.concat(" 00:00:00");
		end = end.concat(" 00:00:00");
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateStart = LocalDateTime.parse(start, format);
		LocalDateTime dateEnd = LocalDateTime.parse(end, format);
		emp.getWorkDays().removeIf(
				w -> !(w.getCheckIn().isAfter(dateStart.minusDays(1)) && w.getCheckIn().isBefore(dateEnd.plusDays(1))));

		return emp;
	}

	@Override
	public List<Employee> getByPeriod(String start, String end) throws ParseException {
		List<Employee> employees = (List<Employee>) jpaEmployeeRepository.findAll();
		start = start.concat(" 00:00:00");
		end = end.concat(" 00:00:00");
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateStart = LocalDateTime.parse(start, format);
		LocalDateTime dateEnd = LocalDateTime.parse(end, format);

		for (Employee emp : employees) {
			emp.getWorkDays().removeIf(w -> !(w.getCheckIn().isAfter(dateStart.minusDays(1))
					&& w.getCheckIn().isBefore(dateEnd.plusDays(1))));
		}
		employees.removeIf(e -> e.getWorkDays().isEmpty());
		return employees;
	}

	@Override
	public List<Employee> getAllByMonth(String month) {
		List<Employee> employees = (List<Employee>) jpaEmployeeRepository.findAll();
		for (Employee emp : employees) {
			emp.getWorkDays().removeIf(w -> !(w.getCheckIn().getMonthValue() == Integer.parseInt(month)));
		}
		employees.removeIf(e -> e.getWorkDays().isEmpty());
		return employees;
	}
}
